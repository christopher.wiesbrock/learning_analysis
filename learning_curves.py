# -*- coding: utf-8 -*-
"""
Created on Tue May 30 15:33:59 2023

@author: wiesbrock
"""

import numpy as np
import matplotlib.pylab as plt
import seaborn as sns
import glob
import pandas as pd

mouse_id=2699
path="D:\Ponzo Daten\\"+str(mouse_id)+'\*.csv'
file_list=glob.glob(path)
file_list=np.sort(file_list)

correct=np.zeros((len(file_list)))
trials=np.zeros((len(file_list)))

for i in range(len(file_list)):
    
    data=pd.read_csv(file_list[i],delimiter=',')
    correct[i]=np.mean(data['correct'])
    trials[i]=len(data['correct'])
    print(i)

plt.figure(dpi=300) 
plt.title('Performance '+str(mouse_id), fontsize=16)   
plt.xlabel('Session',fontsize=14)
plt.ylabel('Performance [%]', fontsize=14)
plt.xticks(np.linspace(1,len(correct),len(correct)))
sns.despine()
x=np.linspace(1,len(correct),len(correct))
plt.plot(x,correct*100, 'k-')

guess_rate=50
plt.plot(x,np.linspace(50,50,len(correct)),'k--', label='guess rate')

disc_thresh=75
plt.plot(x,np.linspace(75,75,len(correct)),'r--', label='discrimination threshold')

plt.plot(x,np.linspace(85,85,len(correct)),'g--', label='Expert level')
plt.legend(loc=2)
#plt.figure()
#plt.title('Trials')
#plt.plot(trials)
